package com.cc.reference;

public class UserRepository {
    
    private final User user;
    
    public UserRepository(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserRepository{" + "user=" + user + '}';
    }
    
}
